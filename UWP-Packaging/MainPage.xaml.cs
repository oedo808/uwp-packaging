﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Security.Cryptography;
using System.Text;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWP_Packaging
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void TxtInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(txtInput.Text.Length > 0)
            {
                btnHash.Visibility = 0;
            }
        }

        private void BtnHash_Click(object sender, RoutedEventArgs e)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(txtInput.Text);
                byte[] hashBytes = md5Hash.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (int i=0; i< hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                txtHashed.Text = sb.ToString();
            }
        }
    }
}
